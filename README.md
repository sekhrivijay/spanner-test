# spanner-test
# Switch between mysql and spanner 

create an instance on spanner called "mytestinstance" 
create a database on that instance called "db_example"
create a table on that database called "user" with this DDL

    CREATE TABLE user (
        id INT64 NOT NULL,
        email STRING(255),
        name STRING(255),
    ) PRIMARY KEY (id)


Generate a service account on GCP with spanner access and download it 
Change application.properties and specify full path to that downloaded key for PvtKeyPath
Change application.properties and specify projectid for Project
Here is ane example 

        spring.datasource.url=jdbc:cloudspanner://localhost;Project=optical-precept-176916;Instance=mytestinstance;Database=db_example;SimulateProductName=PostgreSQL;PvtKeyPath=/pathtp/serviceaccountkey.json;AllowExtendedMode=false


# Build the app 
    ./mwn clean install
# Run the app
     java -jar \target\gs-mysql-data-0.1.0.jar


Go to browser to add a user in spanner database

    http://localhost:8080/demo/add?name=vijay&email=abc@abc.com

Retrive all the user 

    http://localhost:8080/demo/all



# To do the same for mysql 
create a database in mysql
   
     create database db_example
   
edit application.properties file and  
uncomment this line and specify where mysql server is running

    spring.datasource.url=jdbc:mysql://localhost:3306/db_example

uncomment username and password 

    spring.datasource.username=root
    spring.datasource.password=XXXXXXXXXX

Change none to create

    spring.jpa.hibernate.ddl-auto=create

Comment rest of the properties 
  
    spring.datasource.url=jdbc:cloudspanner://localhost;Project=optical-precept-176916;Instance=mytestinstance;Database=db_example;SimulateProductName=PostgreSQL;PvtKeyPath=/Users/vsekhri/apps/gcp/springkey.json;AllowExtendedMode=false
    spring.jpa.show-sql=true
    
    spring.jpa.open-in-view=true
    
    spring.jpa.properties.hibernate.dialect = nl.topicus.hibernate.dialect.CloudSpannerDialect
  
    spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
  
    spring.datasource.driver-class-name=nl.topicus.jdbc.CloudSpannerDriver

Build and run the app 


   